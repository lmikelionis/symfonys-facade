![Build Status](https://codeship.com/projects/1b0ec360-ed12-0132-15ff-6697a529e469/status?branch=master)

![Scrutinizer](https://scrutinizer-ci.com/b/lmikelionis/symfonys-facade/badges/quality-score.png?b=master)
![CodeCoverage](https://scrutinizer-ci.com/b/lmikelionis/symfonys-facade/badges/coverage.png?b=master)
![ScrutinizerBuild](https://scrutinizer-ci.com/b/lmikelionis/symfonys-facade/badges/build.png?b=master)


Symfonys facade for Laravel5
====================

This package lets you use Symfony2 specific bundles inside Laravel5 application.
Simply add you Symfony centric bundle to `composer.json` install it, configure it and enjoy it ;)

 It supporst such features as:
 
 - Symfonys dependency injection container with symfony config files.
 
 - Route porting. Routes that are configured in `routes.yml` files.
 
 - Symfony commands.
 
 
 As this package is still in early beta, not all functions and compatabilities are tested and developed.

Installation
---------------

Add it to composer:

`
dfsdfsd sdfsdf dfssd
`

Configure it:

`confidsdada g uygyg hgvhcvchhg kuggv`

Test it:

` kljlki hkjhkj kjghjhg`

Usage
--------

As an usage example I will use ESB.

It's a nice and easy PDO for ElasticSearch.


License
---------
This bundle is under the MIT license. 

Some other stuff
----------------------

Probably there is natural quastion, why use Symfonys bundles in Larave.
Answer is: Because I want so :D

Yes for libraries, not bundles, there is no need to use such thing.
But in World Wide Web there is many good bundles, that are using actual Symfony framework.

So as I wanted to use Symfony specific bundle in Laravel5 project, that I didnt saw to be ported easily. 
At the end of the day we have this package.

Also this facde can be usefull if in need of fast prototyping. Jus include it, register it and you have Symfony inside Laravel5 ;)